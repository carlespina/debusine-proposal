# What is this document?

I'm reading https://hertzog.pages.debian.net/debusine/devel/why.html (from
https://salsa.debian.org/hertzog/debusine) and trying to understand it.

This is not a software architecture design document. The document only tries to show how users might interact with Debusine and the different components and how they can be integrated.

**Names of components can be changed, technology is not defined (when mentioning one it is *merely as an example*)**.

This document currently is not a detailed plan.

# Technologies

## Communication: HTTP-JSON

Pros:

* Widely supported by any language in case that we want to re-implement or replace parts of the system with other
  languages, libraries
* No need to create system users
* Structured information: pass dictionaries, add elements, arrays, etc.

Cons:

* HTTPS might be problematic if it's used without internet access
* Create and bundle CA in the different systems? Self-signed certs and add exceptions?
* Authentication needs to be handled separately

## Communication: TODO consider other technologies

## Celery

When we talked about workers I thought of Celery and Redis (and similar technologies).

When thinking a bit more I realised that Celery workers seem to be designed to be homogenous (or just a few different queues and workers subscribing to different queues).

In Debusine there is a big variety of worker attributes. We need to send the tasks to the best-suited ones (giving them a mark with how optimal they are?). Debusine workers at the very minimum have attributes such as:
 * RAM
 * HDD
 * Cores
 * Architectures

So it seems that the "Debusine-orchestrator" should know the requirements of a task to be assigned and find the best worker for this (workers wouldn't know the other available workers so they don't know if they are the "best ones"). I suspect that Celery and other systems might not have this functionality. I don't like re-inventing the wheel, but nor do I like using squared-wheels. We need to consider if we go home-made or find something that suits our case for the workers.

# High level overview Debusine
Debusine-core and Debusine-UI (or Debusine-backend/frontend, or...) could be a single Django application with two apps or two Django applications.

Django-UI could use the classes and models from Debusine-core or be built in different technologies (communicating with an API).

There would be more apps in both Debusine-core and Debusine-UI (e.g. Debusine-orchestrator, authentication).

![Main diagram](images/main_diagram.png)

## Debusine-core
There could be "Debusine-core" (implemented in Django) that would interact only with API calls (with plain Django or using django-rest-framework).

Debusine-UI (another Django application) or Debusine-CLI (pure Python) could interact with this system.

## Debusine-UI
A Django application using Bootstrap, Django templates, etc. ("traditional" system) that would use the Debusine Core API to display the status of tasks, queues, manage them, etc.

Side note: I haven't used HTMX (https://htmx.org/), but it's something that I really want to test. It could give us a good balance between simplicity and interactivity on the UI side.

## Debusine CLI
Python scripts that would connect to Debusine core using `requests` (for example). For tasks that are long and asynchronous it would need to be able to list pending tasks from the user (or started by the user) or refer to the Debusine-UI.

# Flow

A Debusine client wants to upload a source package. This could be done via:

* FTP upload
* HTTP upload
* signed tag in a git repository

Let's assume that "Debusine-core" (in Django) has a JSON API and the client (could be a user with a `debusine-client.py`
, git hook from Salsa, etc.) would execute something along these lines:

## Create source package
```shell
debusine-client.py create source_package git://server/repo@tag
 architectures=amd64,arm64,i386
 minimum_memory=8GB
 minimum_hard_disk=16G
```
Will return a workflow ID.

This will trigger a workflow (named `source_package`) that will execute tasks as per its configuration (saved in the central Git workflow/tasks configuration):
 * Create task `fetch_source_package` passing JSON which will contain the git path (or FTP PATH upload?). The task's definition will create an environment (chroot, using the task configuration process), fetch the code, create an `artifact1.tgz`, attach JSON (with information like user that invoked it, date and time, time of the operation, hash of the package, log?, etc.). The task would save the artifact in the object storage and create a new DB entry with the artifact path and associated JSON. Return the source_package ID (or UUID)
 * Create a task `validate_source_package` which will fetch the artifact, uncompress and validate certain things (e.g. use Debian linters to ensure that the package has a LICENSE, etc.)

The workflow (or pipeline?) is a planning execution defining tasks with its dependencies. When all the dependencies of a task have finished, the task can start.

Each task will have access to the Debusine "Task" module in order to do operations like "save this directory into the object storage", "fetch the artifact", "attach metadata to the task", "fetch last result", "mark as failure", "mark as success", "update progress", etc. This can be exposed via CLI commands in the tasks environment, to be used from shell scripts (if tasks are executed in a shell environment; same concept if they were Python code).

Worfklows and task code could be defined in JSON or YAML and be saved into a Git repository. The new configuration would be deployed when needed.

I'm quite familiar with the GitHub Actions way of interacting (probably similar to Salsa) so a way to ingest secrets, updates on the progress, etc. could be similar.

## Create binary package
Then the user could do:
```shell
debusine-client.py create binary_package source_package_id=$SEE_PREVIOUS_STEP
```

Will return a workflow ID.

This will trigger a new workflow (named `binary_package`) that will (as defined in the workflows+tasks repo):
 * Create one task per architecture `create_binary_package_$ARCH`:  will fetch the source package artifact, set up the environment as needed (chroot?) and initiate the creation of the binary package (`debian/rules`?). When finished, it will package the result into an artifact: save it into the object storage and attach metadata. The output of each `create_binary_package_$ARCH` is the artifact in the object storage and the JSON metadata stored in the database
 * Publish `publish_package`: when all the architecture tasks have finished the publish step starts: upload each of the artifacts to the "Final Repository" (see drawing). This task would not have an output artifact but would have metadata (logs of the upload, time, etc.)

(the packaging into artifact step: the https://hertzog.pages.debian.net/debusine/devel/design.html document says that files can be added; for simplicity I'm thinking at the moment of directories)

* `artifact1.tgz` in the object storage
* Create a DB entry with the `A1.tgz` path and the associated metadata
  (either in different fields in the DB or in a JSON key/value; depending on the DB and query capabilities needed afterwards)

# Roles in Debusine

Debusine should be generic enough to be used by Kali and Debian, as well as anyone else that needs workflows, tasks, storage, etc.

## Debusine developer
Developers of Debusine should understand the needs but might not know all the details of the workflows implemented in different "distros" (or different situations).

## Debusine system administrator
The system administrator sets up Debusine in an organisation: creates the workflows and task types, gives permissions, integrates it with existing systems, etc.

(note that the system administrator could be two roles: setting up the systems and setting up the workflows)

## Debusine end-user
User of Debusine (via CLI or Web) not involved in the setup of workflows, tasks, etc.

Debusine documentation says that "we want to have humans review the output of all the automated checks and have them approve the upload to the target repository". Using Debusine-UI a human could interact and approve, but this could also be done using Debusine-CLI (information might be easier to display in a web page though). On approval, new metadata for the artifact would be added and then a new task might be initiated (or a workflow?).

# Similar systems
## Renku
When I was reading Debusine I couldn't stop thinking of https://renkulab.io/ (I'm quite familiar with it) or https://wholetale.org/ (not familiar but same objectives as Renku).

Renku is designed to help researchers to do reproducible science.

Renku's principles are:
 * Researchers upload data into it (it gets stored in Git, Git+LFS)
 * Researchers can interact with a CLI Python tool or a web-based interface
 * Researchers upload code into Renku
 * Renku can create an environment (based on Docker) that gets a "set of data" + "executes the code" -> generates "new set of data"
 * At any time other researchers can do a fork and do their own code modifications

Renku also has Python Notebooks and systems to publish data. To publish data is to export the result into data repositories like https://zenodo.org/

It wouldn't be useful to reuse Renku code but I always thought that it has things in common with Debusine.

## Automation servers
Jenkins, GitHub Actions, Salsa actions (as already explained in Debusine documentation) also have some similarities. Debusine would have a heavier use of artifacts and metadata in JSON.

# Development
 * 100% TDD
 * Use Black for code formatting
 * Consider Python types (some people dislike them but I don't see any good reason to not use it for a project like this... unless the parts touching Django end up having too many "Any" everywhere. Types are easy to remove if we don't find them useful)
 * I think that having a staging environment from the beginning might be useful: anyone (but I am thinking of the team involved in this) can see the evolution without installing and setting it up; or even installing and setting up a common instance to show different cases might be useful

# Next steps
 * Discuss technologies (it might need research)
 * Need to think about authentication (e.g. who can review the output of all the automated checks and approve the upload? How is the person authenticated?)
 * Decide what deliverables make sense for Kali: either in a logical way towards the Debusine "complete" or in a way that it can start replacing existing elements to exercise part of the new system